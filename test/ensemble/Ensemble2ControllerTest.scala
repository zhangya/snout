package ensemble

import org.scalatest.FunSuite

class Ensemble2ControllerTest extends FunSuite {
  test("formatName") {
    assert(Ensemble2Controller.formatName("abcDefGhi") == "Abc Def Ghi")
    assert(Ensemble2Controller.formatName("abcDefGhiSample") == "Abc Def Ghi")
    assert(Ensemble2Controller.formatName("abcDefGhi Sample") == "Abc Def Ghi")
  }
}
