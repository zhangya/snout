package ensemble

import javafx.scene.input.MouseEvent
import javafx.event.{Event, EventHandler, ActionEvent}
import net.oschina.oldpig.snout.Animate
import scala.concurrent.duration._
import javafx.beans.Observable
import javafx.scene.control.TreeItem
import javafx.beans.value.ObservableValue
import javafx.scene.Node
import javafx.scene.layout.Region
import ensemble.pages.{Pages, Page}
import scala.collection.mutable


trait Ensemble2Controller {view: Ensemble2View.type =>
  var changingPage = false

  val forwardHistory = mutable.Stack.empty[Page]
  val history = mutable.Stack.empty[Page]

  def onPageTreeSelectedChanged(ob: ObservableValue[_ <: TreeItem[String]], old: TreeItem[String], _new: TreeItem[String]) {
    if (!changingPage) {
      val selected = pageTree.getSelectionModel.getSelectedItem.asInstanceOf[Page]
      if (selected != Pages.root)
        gotoPage(selected)
    }
  }

  def onGotoPage(path: String) = new EventHandler[Event] {
    def handle(e: Event) {
      gotoPage(path)
    }
  }

  def onModalDimmerClick(e: MouseEvent) = {
    e.consume()
    hideModalMessages()
  }

  def onHighlightsButtonClick(e: ActionEvent) {
    gotoPage(Pages.highlightedSamples)
  }

  def onNewButtonClick(e: ActionEvent) {
    gotoPage(Pages.newSamples)
  }

  def treeButtonNotifyListener(ob: Observable) = {
    pageTree.setRoot(
      if (allButton.isSelected)
        Pages.root
      else if (samplesButton.isSelected)
        Pages.samples
      else // if (docsButton.isSelected)
        Pages.docs
    )
  }

  def back(e: ActionEvent) {
  }

  def forward(e: ActionEvent) {
  }

  def reload(e: ActionEvent) {
  }

  def showProxyDialog(e: ActionEvent) {
  }

  def gotoPage(pagePath: String): Unit = Pages.getPage(pagePath)

  def gotoPage(page: Page): Unit = {
    gotoPage(page, addHistory = true, force = false, swapViews = true)
  }

  var currentPage: Page = _

  var currentPageView: Node = _

  def gotoPage(page: Page, addHistory: Boolean, force: Boolean, swapViews: Boolean): Unit = {
    if (!(page == null || (!force && page == currentPage))) {
      changingPage = true
      if (swapViews) {
        val view = page.createView
        if (force || view != currentPageView){

        }
      }
    }
  }

  def hideModalMessages(): Unit = {
    modalDimmer.setCache(true)
    import Animate._
    playAnimate(
      at(1.second)(modalDimmer.opacityProperty easeOutTo 0) {
        e: ActionEvent =>
          modalDimmer.setCache(false)
          modalDimmer.setVisible(false)
          modalDimmer.getChildren.clear()
      }
    )
  }

  def onResizePressed(e: MouseEvent) {
  }

  def onResizeDragged(e: MouseEvent) {
  }



}

object Ensemble2Controller {
  def formatName(packageName:String) = {
    val removeSample = if (packageName.endsWith("Sample")) packageName.substring(0, packageName.length - "Sample".length) else packageName
    removeSample.replaceAll("([\\p{Upper}\\d])", " $1").capitalize.trim
  }

}
