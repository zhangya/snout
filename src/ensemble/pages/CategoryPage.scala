package ensemble.pages

import javafx.scene.layout.VBox
import net.oschina.oldpig.snout.FxBuilder._
import javafx.scene.control.Control
import net.oschina.oldpig.snout.NodeFactory

object Category extends NodeFactory[VBox] {
  def createInstance = new VBox() {
    override def computePrefHeight(width: Double) = {
      super.computePrefHeight(width)
      getParent.getBoundsInLocal.getHeight
    }
  }
}


case class CategoryPage(name: String, pages: Page*) extends Page(name) {

  import collection.JavaConverters._

  getChildren.addAll(pages: _*)

  val (directChildren, categoryChildren) = getChildren.asScala.span(_.isInstanceOf[SamplePage])

  private def addAllCategoriesSampleTiles(category: CategoryPage): Unit = {
    category.getChildren.asScala.foreach {
      case sample: SamplePage => sample.createTile
      case cat: CategoryPage => addAllCategoriesSampleTiles(cat)
    }
  }

  override def createView = scrollPane(styleClass := "noborder-scroll-pane", fitToWidth := true){
    Category(spacing := 8, styleClass := "category-pane") {
      label(/*maxWidth := Double.MaxValue, text := name, minHeight := Control.USE_PREF_SIZE, styleClass := "page-header"*/)
      if (directChildren.nonEmpty) {
        tilePane(hgap := 8, vgap := 8, prefColumns := 1, styleClass := "category-page-flow") {
          directChildren.map(_.asInstanceOf[SamplePage].createTile)
        }
      }
      categoryChildren.foreach { case categoryPage: CategoryPage =>
        label(/*text := categoryPage.name, maxWidth := Double.MaxValue, minHeight := Control.USE_PREF_SIZE, styleClass := "category-header"*/)
        tilePane(hgap := 8, vgap := 8, prefColumns := 1, styleClass := "category-page-flow") {
          addAllCategoriesSampleTiles(categoryPage)
        }
      }
    }
  }
}
