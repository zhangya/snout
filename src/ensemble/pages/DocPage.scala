package ensemble.pages

import javafx.scene.layout.Pane
import javafx.collections.FXCollections
import javafx.scene.web.WebView
import net.oschina.oldpig.snout.FxBuilder._
import javafx.scene.control.ScrollPane

case class DocPage(name: String, docUrl: String) extends Page(name) {
  val relatedSamples = FXCollections.observableArrayList[SamplePage]()
  var anchor: String = _

  override def createView = {
    DocPane.docPage = this
    DocPane
  }
}

object DocPane extends Pane {
  var docPage: DocPage = _
  val wbView = is[WebView]
  val sidePane = is[ScrollPane]

  override def layoutChildren() {
    val w = getWidth
    val h = getHeight
    val sideWidth = 70
    if (docPage != null && !docPage.relatedSamples.isEmpty) {
      wbView.resizeRelocate(0, 0, w - sideWidth, h)
      sidePane.resizeRelocate(w - sideWidth, 0, sideWidth, h)
    } else {
      wbView.resizeRelocate(0, 0, w, h)
    }
  }

  builderParent.withValue(this) {
    webView(contextMenuEnabled:=false) as wbView
    scrollPane(styleClass:="noborder-scroll-pane", fitToWidth:=true)
  }


}
