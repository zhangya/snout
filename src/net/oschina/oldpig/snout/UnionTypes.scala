package net.oschina.oldpig.snout

import javafx.scene.Node

import scala.language.implicitConversions
/**
 * Reference Only. Not used in snout.
 */
object UnionTypes {
  type ¬[A] =  A => Nothing
  type ∨[T, U] = ¬[¬[T] with ¬[U]]
  type ¬¬[A] = ¬[¬[A]]
  type |∨|[T, U] = { type L[X] = ¬¬[X] <:< (T ∨ U) }


  type or[A, B] = Either[A, B]

  implicit def firstOf2[A, B](a: A): or[A, B] = Left(a)

  implicit def secondOf2[A, B](b: B): or[A, B] = Right(b)

  implicit def singleFrom2[A, B](v: A or B) = v match {
    case Left(a) => a
    case Right(b) => b
  }

  implicit def firstOf3[A, B, C](a: A): A or B or C = Left(Left(a))

  implicit def secondOf3[A, B, C](b: B): A or B or C = Left(Right(b))

  implicit def thirdOf3[A, B, C](c: C): A or B or C = Right(c)

  implicit def singleFrom3[A, B, C](v: A or B or C) = v match {
    case Left(Left(a)) => a
    case Left(Right(b)) => b
    case Right(c) => c
  }

  implicit def firstOf4[A, B, C, D](a: A): or[or[or[A, B], C], D] = Left(Left(Left(a)))

  implicit def secondOf4[A, B, C, D](b: B): or[or[or[A, B], C], D] = Left(Left(Right(b)))

  implicit def thirdOf4[A, B, C, D](c: C): or[or[or[A, B], C], D] = Left(Right(c))

  implicit def fourthOf4[A, B, C, D](d: D): or[or[or[A, B], C], D] = Right(d)

  implicit def singleFrom4[A, B, C, D](v: A or B or C or D) = v match {
    case Left(Left(Left(a))) => a
    case Left(Left(Right(b))) => b
    case Left(Right(c)) => c
    case Right(d) => d
  }

  implicit def firstOf5[A, B, C, D, E](a: A): A or B or C or D or E = Left(Left(Left(Left(a))))

  implicit def secondOf5[A, B, C, D, E](b: B): A or B or C or D or E = Left(Left(Left(Right(b))))

  implicit def thirdOf5[A, B, C, D, E](c: C): A or B or C or D or E = Left(Left(Right(c)))

  implicit def fourthOf5[A, B, C, D, E](d: D): A or B or C or D or E = Left(Right(d))

  implicit def fifthOf5[A, B, C, D, E](e: E): A or B or C or D or E = Right(e)

  implicit def singleFrom5[A, B, C, D, E](v: A or B or C or D or E) = v match {
    case Left(Left(Left(Left(a)))) => a
    case Left(Left(Left(Right(b)))) => b
    case Left(Left(Right(c))) => c
    case Left(Right(d)) => d
    case Right(e) => e
  }

  implicit def firstOf6[A, B, C, D, E, F](a: A): A or B or C or D or E or F = Left(Left(Left(Left(Left(a)))))

  implicit def secondOf6[A, B, C, D, E, F](b: B): A or B or C or D or E or F = Left(Left(Left(Left(Right(b)))))

  implicit def thirdOf6[A, B, C, D, E, F](c: C): A or B or C or D or E or F = Left(Left(Left(Right(c))))

  implicit def fourthOf6[A, B, C, D, E, F](d: D): A or B or C or D or E or F = Left(Left(Right(d)))

  implicit def fifthOf6[A, B, C, D, E, F](e: E): A or B or C or D or E or F = Left(Right(e))

  implicit def sixthOf6[A, B, C, D, E, F](f: F): A or B or C or D or E or F = Right(f)

  implicit def singleFrom6[A, B, C, D, E, F](v: A or B or C or D or E or F) = v match {
    case Left(Left(Left(Left(Left(a))))) => a
    case Left(Left(Left(Left(Right(b))))) => b
    case Left(Left(Left(Right(c)))) => c
    case Left(Left(Right(d))) => d
    case Left(Right(e)) => e
    case Right(f) => f
  }

  implicit def firstOf7[A, B, C, D, E, F, G](a: A): A or B or C or D or E or F or G = Left(Left(Left(Left(Left(Left(a))))))

  implicit def secondOf7[A, B, C, D, E, F, G](b: B): A or B or C or D or E or F or G = Left(Left(Left(Left(Left(Right(b))))))

  implicit def thirdOf7[A, B, C, D, E, F, G](c: C): A or B or C or D or E or F or G = Left(Left(Left(Left(Right(c)))))

  implicit def fourthOf7[A, B, C, D, E, F, G](d: D): A or B or C or D or E or F or G = Left(Left(Left(Right(d))))

  implicit def fifthOf7[A, B, C, D, E, F, G](e: E): A or B or C or D or E or F or G = Left(Left(Right(e)))

  implicit def sixthOf7[A, B, C, D, E, F, G](f: F): A or B or C or D or E or F or G = Left(Right(f))

  implicit def seventhOf7[A, B, C, D, E, F, G](g: G): A or B or C or D or E or F or G = Right(g)

  implicit def singleFrom7[A, B, C, D, E, F, G](v: A or B or C or D or E or F or G) = v match {
    case Left(Left(Left(Left(Left(Left(a)))))) => a
    case Left(Left(Left(Left(Left(Right(b)))))) => b
    case Left(Left(Left(Left(Right(c))))) => c
    case Left(Left(Left(Right(d)))) => d
    case Left(Left(Right(e))) => e
    case Left(Right(f)) => f
    case Right(g) => g
  }

  implicit def firstOf8[A, B, C, D, E, F, G, H](a: A): A or B or C or D or E or F or G or H = Left(Left(Left(Left(Left(Left(Left(a)))))))

  implicit def secondOf8[A, B, C, D, E, F, G, H](b: B): A or B or C or D or E or F or G or H = Left(Left(Left(Left(Left(Left(Right(b)))))))

  implicit def thirdOf8[A, B, C, D, E, F, G, H](c: C): A or B or C or D or E or F or G or H = Left(Left(Left(Left(Left(Right(c))))))

  implicit def fourthOf8[A, B, C, D, E, F, G, H](d: D): A or B or C or D or E or F or G or H = Left(Left(Left(Left(Right(d)))))

  implicit def fifthOf8[A, B, C, D, E, F, G, H](e: E): A or B or C or D or E or F or G or H = Left(Left(Left(Right(e))))

  implicit def sixthOf8[A, B, C, D, E, F, G, H](f: F): A or B or C or D or E or F or G or H = Left(Left(Right(f)))

  implicit def seventhOf8[A, B, C, D, E, F, G, H](g: G): A or B or C or D or E or F or G or H = Left(Right(g))

  implicit def eighthOf8[A, B, C, D, E, F, G, H](h: H): A or B or C or D or E or F or G or H = Right(h)

  implicit def singleFrom8[A, B, C, D, E, F, G, H](v: A or B or C or D or E or F or G or H) = v match {
    case Left(Left(Left(Left(Left(Left(Left(a))))))) => a
    case Left(Left(Left(Left(Left(Left(Right(b))))))) => b
    case Left(Left(Left(Left(Left(Right(c)))))) => c
    case Left(Left(Left(Left(Right(d))))) => d
    case Left(Left(Left(Right(e)))) => e
    case Left(Left(Right(f))) => f
    case Left(Right(g)) => g
    case Right(h) => h
  }

}

sealed trait OneOf3[A, B, C]

case class Type1Of3[A, B, C](v: A) extends OneOf3[A, B, C]

case class Type2Of3[A, B, C](v: B) extends OneOf3[A, B, C]

case class Type3Of3[A, B, C](v: C) extends OneOf3[A, B, C]

object OneOf3 {
  implicit def OneOf3_2_Node[A <: Node, B <: Node, C <: Node](v: OneOf3[A, B, C]): Node = v match {
    case Type1Of3(a) => a
    case Type2Of3(b) => b
    case Type3Of3(c) => c
  }
}

sealed trait OneOf4[A, B, C, D]

case class Type1Of4[A, B, C, D](v: A) extends OneOf4[A, B, C, D]

case class Type2Of4[A, B, C, D](v: B) extends OneOf4[A, B, C, D]

case class Type3Of4[A, B, C, D](v: C) extends OneOf4[A, B, C, D]

case class Type4Of4[A, B, C, D](v: D) extends OneOf4[A, B, C, D]

object OneOf4 {
  implicit def OneOf4_2_Node[A <: Node, B <: Node, C <: Node, D <: Node](v: OneOf4[A, B, C, D]): Node = v match {
    case Type1Of4(a) => a
    case Type2Of4(b) => b
    case Type3Of4(c) => c
    case Type4Of4(d) => d
  }
}

sealed trait OneOf8[A, B, C, D, E, F, G, H]

case class Type1Of8[A, B, C, D, E, F, G, H](v: A) extends OneOf8[A, B, C, D, E, F, G, H]

case class Type2Of8[A, B, C, D, E, F, G, H](v: B) extends OneOf8[A, B, C, D, E, F, G, H]

case class Type3Of8[A, B, C, D, E, F, G, H](v: C) extends OneOf8[A, B, C, D, E, F, G, H]

case class Type4Of8[A, B, C, D, E, F, G, H](v: D) extends OneOf8[A, B, C, D, E, F, G, H]

case class Type5Of8[A, B, C, D, E, F, G, H](v: E) extends OneOf8[A, B, C, D, E, F, G, H]

case class Type6Of8[A, B, C, D, E, F, G, H](v: F) extends OneOf8[A, B, C, D, E, F, G, H]

case class Type7Of8[A, B, C, D, E, F, G, H](v: G) extends OneOf8[A, B, C, D, E, F, G, H]

case class Type8Of8[A, B, C, D, E, F, G, H](v: H) extends OneOf8[A, B, C, D, E, F, G, H]

object OneOf8 {
  implicit def OneOf8_2_Node[A <: Node, B <: Node, C <: Node, D <: Node, E <: Node, F <: Node, G <: Node, H <: Node](v: OneOf8[A, B, C, D, E, F, G, H]): Node = v match {
    case Type1Of8(a) => a
    case Type2Of8(b) => b
    case Type3Of8(c) => c
    case Type4Of8(d) => d
    case Type5Of8(e) => e
    case Type6Of8(f) => f
    case Type7Of8(g) => g
    case Type8Of8(h) => h
  }
}
