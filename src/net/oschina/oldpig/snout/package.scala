package net.oschina.oldpig

import scala.util.DynamicVariable
import javafx.scene.{Node, DepthTest, Parent}
import javafx.geometry.{Insets, Pos}
import javafx.scene.control.{SelectionMode, TreeItem, ContentDisplay, ToggleGroup}
import javafx.stage.WindowEvent
import javafx.scene.input.{KeyEvent, MouseEvent}
import javafx.event.ActionEvent
import javafx.scene.image.Image
import javafx.scene.layout.Priority

package object snout {
  type BuilderParent = DynamicVariable[Parent]

  implicit class Alignment(val v: Pos) extends AnyVal
  implicit class Padding(val v: Insets) extends AnyVal
  implicit class Closable(val v: Boolean) extends AnyVal
  implicit class StyleClass(val v: Seq[String]) extends AnyVal
  implicit class SDividerPosition(val v: (Int, Double)) extends AnyVal
  implicit class FillHeight(val v: Boolean) extends AnyVal

  implicit class FillWidth(val v: Boolean) extends AnyVal
  implicit class FitToWidth(val v: Boolean) extends AnyVal
  implicit class SDepthTest(val v: DepthTest) extends AnyVal
  implicit class PromptText(val v: String) extends AnyVal
  implicit class Content(val v: Node) extends AnyVal
  implicit class MinSize(val v: (Double, Double)) extends AnyVal
  implicit class WrapText(val v: Boolean) extends AnyVal
  implicit class Visible(val v: Boolean) extends AnyVal
  implicit class Text(val v: String) extends AnyVal
  implicit class SToggleGroup(val v: ToggleGroup) extends AnyVal
  implicit class OnHidden(val v: WindowEvent => Unit) extends AnyVal
  implicit class OnMouseClicked(val v: MouseEvent => Unit) extends AnyVal
  implicit class OnMousePressed(val v: MouseEvent => Unit) extends AnyVal
  implicit class OnKeyReleased(val v: KeyEvent => Unit) extends AnyVal
  implicit class OnMouseDragged(val v: MouseEvent => Unit) extends AnyVal
  implicit class OnAction(val v: ActionEvent => Unit) extends AnyVal
  implicit class SContentDisplay(val v: ContentDisplay) extends AnyVal
  implicit class Spacing(val v: Int) extends AnyVal
  implicit class MinHeight(val v: Double) extends AnyVal
  implicit class MaxHeight(val v: Double) extends AnyVal
  implicit class MinWidth(val v: Double) extends AnyVal
  implicit class MaxWidth(val v: Double) extends AnyVal
  implicit class PrefWidth(val v: Double) extends AnyVal
  implicit class PrefHeight(val v: Double) extends AnyVal
  implicit class MaxSize(val v: (Double, Double)) extends AnyVal
  implicit class PrefSize(val v: (Double, Double)) extends AnyVal
  implicit class PrefColumns(val v: Int) extends AnyVal
  implicit class Id(val v: String) extends AnyVal
  implicit class Graphic(val v: String) extends AnyVal
  implicit class ShowRoot(val v: Boolean) extends AnyVal
  implicit class Hgap(val v: Double) extends AnyVal
  implicit class Vgap(val v: Double) extends AnyVal
  implicit class Selected(val v: Boolean) extends AnyVal
  implicit class ContextMenuEnabled(val v: Boolean) extends AnyVal
  implicit class SImage(val v: Image) extends AnyVal
  implicit class Root[T](val v: TreeItem[T]) extends AnyVal
  implicit class Editable(val v: Boolean) extends AnyVal
  implicit class SSelectionMode(val v: SelectionMode) extends AnyVal

  implicit class Margin(val v:Insets) extends AnyVal
  implicit class Hgrow(val v:Priority) extends AnyVal
  implicit class SConstraints(val v:(Int, Int)) extends AnyVal
  implicit class Vgrow(val v:Priority) extends AnyVal
}
