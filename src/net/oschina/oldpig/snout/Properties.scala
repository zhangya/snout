package net.oschina.oldpig.snout

import javafx.scene.control.{TreeView, Toggle, TextField}

trait Properties {
  lazy val textProperty = { p: TextField => p.textProperty() }
  lazy val selectedProperty = { t: Toggle => t.selectedProperty() }
  def selectedItemProperty[T] = {t: TreeView[T] => t.getSelectionModel.selectedItemProperty() }

}
