package net.oschina.oldpig.snout

import javafx.scene.layout._
import javafx.scene.Node

trait CanSet[ParentType, WrapperType] {
  def transform(v: WrapperType): Node => Unit
}
trait StaticAttribute[ParentType, ValueType, WrapperType]{
  def :=(v: ValueType)(implicit transformer: CanSet[ParentType, WrapperType], wrapper: ValueType => WrapperType): Node => Unit = {
    transformer.transform(v)
  }
}

trait StaticAttributes {

  implicit object HBoxCanSetMargin extends (HBox CanSet Margin) {
    def transform(v: Margin): Node => Unit = { node =>
      HBox.setMargin(node, v.v)
    }
  }

  implicit object BorderPaneCanSetMargin extends (BorderPane CanSet Margin) {
    def transform(v: Margin): Node => Unit = { node =>
      BorderPane.setMargin(node, v.v)
    }
  }
  implicit object FlowPaneCanSetMargin extends (FlowPane CanSet Margin) {
    def transform(v: Margin): Node => Unit = { node =>
      FlowPane.setMargin(node, v.v)
    }
  }
  implicit object GridPaneCanSetMargin extends (GridPane CanSet Margin) {
    def transform(v: Margin): Node => Unit = { node =>
      GridPane.setMargin(node, v.v)
    }
  }
  implicit object TilePaneCanSetMargin extends (TilePane CanSet Margin) {
    def transform(v: Margin): Node => Unit = { node =>
      TilePane.setMargin(node, v.v)
    }
  }
  implicit object StackPaneCanSetMargin extends (StackPane CanSet Margin) {
    def transform(v: Margin): Node => Unit = { node =>
      StackPane.setMargin(node, v.v)
    }
  }
  implicit object VBoxCanSetMargin extends (VBox CanSet Margin) {
    def transform(v: Margin): Node => Unit = { node =>
      TilePane.setMargin(node, v.v)
    }
  }



  implicit object HBoxCanSetHgrow extends (HBox CanSet Hgrow) {
    def transform(v: Hgrow): Node => Unit = { node =>
      HBox.setHgrow(node, v.v)
    }
  }
  implicit object GridPaneCanSetHgrow extends (GridPane CanSet Hgrow) {
    def transform(v: Hgrow): Node => Unit = { node =>
      GridPane.setHgrow(node, v.v)
    }
  }


  implicit object GridPaneCanSetConstraints extends (GridPane CanSet SConstraints) {
    def transform(v: SConstraints): Node => Unit = { node =>
      GridPane.setConstraints(node, v.v._1, v.v._2)
    }
  }

  implicit object VBoxCanSetVgrow extends (VBox CanSet Vgrow){
    def transform(v: Vgrow): Node => Unit = { node =>
      VBox.setVgrow(node, v.v)
    }
  }
}
