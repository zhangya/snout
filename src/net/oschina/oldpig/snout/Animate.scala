package net.oschina.oldpig.snout

import javafx.animation.{Interpolator, KeyValue, KeyFrame, Timeline}
import javafx.beans.property.DoubleProperty
import javafx.event.{EventHandler, ActionEvent}
import scala.concurrent.duration.Duration
import javafx.beans.value.WritableValue

object Animate {

  implicit class STimeline(timeline: Timeline) {
    def keyframes(frames: KeyFrame*) = {
      timeline.getKeyFrames.addAll(frames: _*)
      timeline
    }
  }

  implicit class SWritableValue[T](property: WritableValue[T]) {
    def easeOutTo(v: T): KeyValue = new KeyValue(property.asInstanceOf[WritableValue[T]], v, Interpolator.EASE_OUT)

    def easeInTo(v: T): KeyValue = new KeyValue(property.asInstanceOf[WritableValue[T]], v, Interpolator.EASE_IN)

    def easeBothTo(v: T): KeyValue = new KeyValue(property.asInstanceOf[WritableValue[T]], v, Interpolator.EASE_BOTH)

    def to(v: T): KeyValue = new KeyValue(property.asInstanceOf[WritableValue[T]], v)

    def splineTo(v: T)(x1: Double, y1: Double, x2: Double, y2: Double) = new KeyValue(property.asInstanceOf[WritableValue[T]], v, Interpolator.SPLINE(x1, y1, x2, y2))

    def tangentTo(v: T)(duration: Duration, tang: Double) = new KeyValue(property.asInstanceOf[WritableValue[T]], v, Interpolator.TANGENT(duration, tang))

    def tangentTo(v: T)(d1: Duration, t1: Double, d2: Duration, t2: Double) = new KeyValue(property.asInstanceOf[WritableValue[T]], v, Interpolator.TANGENT(d1, t1, d2, t2))
  }

  object timeline {
    def apply(attributes: (Timeline => Unit)*): Timeline = {
      val result = new Timeline
      attributes.foreach(_.apply(result))
      result
    }
  }

  def at(duration: concurrent.duration.Duration)(delta: KeyValue*)(onFinished: ActionEvent => Unit) =
    new KeyFrame(duration, new EventHandler[ActionEvent]() {
      override def handle(e: ActionEvent): Unit = onFinished.apply(e)
    }, delta: _*)

  def playAnimate(keyframes: KeyFrame*) = timeline().keyframes(keyframes: _*).play()

  implicit def scalaDurationAsFxDuration(v: Duration): javafx.util.Duration = new javafx.util.Duration(v.toMillis)

}
