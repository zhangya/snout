package net.oschina.oldpig.snout

import javafx.geometry.{Pos, Insets}
import javafx.scene.layout._
import javafx.scene.{layout, DepthTest, Node}
import javafx.scene.control._
import javafx.scene.input.{KeyEvent, MouseEvent}
import javafx.event.{EventHandler, ActionEvent}
import javafx.scene.image.{Image, ImageView}
import java.io.InputStream
import javafx.stage.{Window, WindowEvent}
import javafx.beans.value.{ChangeListener, ObservableValue}
import javafx.beans.{Observable, InvalidationListener}
import javafx.scene.web.WebView

trait HasAttribute[-NodeType, WrapperType] {
  def transform(v: WrapperType): NodeType => Unit
}
trait FxAttribute[ValueType, WrapperType] {
  def :=[NodeType](v: ValueType)(implicit transformer: HasAttribute[NodeType, WrapperType], wrapper: ValueType => WrapperType): NodeType => Unit = {
    transformer.transform(v)
  }
}

trait FxAttributes extends StaticAttributes {
  type Has[NodeType , WrapperType] = HasAttribute[NodeType, WrapperType]
  //==================================================================================

  implicit object HBoxHasAlignment extends (HBox Has Alignment) {
    def transform(v: Alignment): HBox => Unit = { hBox =>
      hBox.setAlignment(v.v)
    }
  }

  implicit object VBoxHasAlignment extends (VBox Has Alignment) {
    def transform(v: Alignment): VBox => Unit = { vBox =>
      vBox.setAlignment(v.v)
    }
  }

  implicit object LabeledHasAlignment extends (Labeled Has Alignment) {
    def transform(v: Alignment): Labeled => Unit = { labeled =>
      labeled.setAlignment(v.v)
    }
  }

  implicit object TextFieldHasAlignment extends (TextField Has Alignment) {
    def transform(v: Alignment): TextField => Unit = { textField =>
      textField.setAlignment(v.v)
    }
  }

  implicit object GridPaneHasAlignment extends (GridPane Has Alignment) {
    def transform(v: Alignment): GridPane => Unit = { gridPane =>
      gridPane.setAlignment(v.v)
    }
  }


  implicit object TilePaneHasAlignment extends (TilePane Has Alignment) {
    def transform(v: Alignment): TilePane => Unit = { tilePane =>
      tilePane.setAlignment(v.v)
    }
  }

  implicit object FlowPaneHasAlignment extends (FlowPane Has Alignment) {
    def transform(v: Alignment): FlowPane => Unit = { flowPane =>
      flowPane.setAlignment(v.v)
    }
  }

  implicit object StackPaneHasAlignment extends (StackPane Has Alignment) {
    def transform(v: Alignment): StackPane => Unit = { stackPane =>
      stackPane.setAlignment(v.v)
    }
  }

  object alignment extends FxAttribute[Pos, Alignment]

  //==================================================================================

  implicit object RegionHasPadding extends (Region Has Padding) {
    def transform(v: Padding): Region => Unit = { region =>
      region.setPadding(v.v)
    }
  }

  object padding extends FxAttribute[Insets, Padding] {
    def :=[T](v: (Int, Int, Int, Int))(implicit transformer: HasAttribute[T, Padding], wrapper: Insets => Padding): T => Unit = :=[T](new Insets(v._1, v._2, v._3, v._4))(transformer, wrapper)
  }

  //==================================================================================

  implicit object TabHasClosable extends (Tab Has Closable) {
    def transform(v: Closable): Tab => Unit = { tab =>
      tab.setClosable(v.v)
    }
  }

  object closable extends FxAttribute[Boolean, Closable]

  //==================================================================================

  object styleClass extends FxAttribute[Seq[String], StyleClass] {
    def :=[T](v: String)(implicit transformer: HasAttribute[T, StyleClass], wrapper: Seq[String] => StyleClass): T => Unit = :=[T](Seq(v))(transformer, wrapper)
  }

  implicit object NodeHasStyleClass extends (Node Has StyleClass) {
    def transform(v: StyleClass): Node => Unit = { node =>
      node.getStyleClass.addAll(v.v: _*)
    }
  }

  //==================================================================================

  implicit object SplitPaneHasDividerPosition extends (SplitPane Has SDividerPosition) {
    def transform(v: SDividerPosition): SplitPane => Unit = { dividerPosition =>
      dividerPosition.setDividerPosition(v.v._1, v.v._2)
    }
  }

  object dividerPosition extends FxAttribute[(Int, Double), SDividerPosition]

  //==================================================================================

  implicit object HBoxHasFillHeight extends (HBox Has FillHeight) {
    def transform(v: FillHeight): HBox => Unit = { fillHeight =>
      fillHeight.setFillHeight(v.v)
    }
  }

  object fillHeight extends FxAttribute[Boolean, FillHeight]

  //==================================================================================

  implicit object VBoxHasFillWidth extends (VBox Has FillWidth) {
    def transform(v: FillWidth): VBox => Unit = { vBox =>
      vBox.setFillWidth(v.v)
    }
  }

  object fillWidth extends FxAttribute[Boolean, FillWidth]

  //==================================================================================


  implicit object ScrollPaneHasFitToWidth extends (ScrollPane Has FitToWidth) {
    def transform(v: FitToWidth): ScrollPane => Unit = { scrollPane =>
      scrollPane.setFitToWidth(v.v)
    }
  }

  object fitToWidth extends FxAttribute[Boolean, FitToWidth]

  //==============================================================================

  implicit object NodeHasDepthTest extends (Node Has SDepthTest) {
    def transform(v: SDepthTest): Node => Unit = { node =>
      node.setDepthTest(v.v)
    }
  }

  object depthTest extends FxAttribute[DepthTest, SDepthTest]

  //==============================================================================

  implicit object TestFieldHasPromptText extends (TextField Has PromptText) {
    def transform(v: PromptText): TextField => Unit = { textField =>
      textField.setPromptText(v.v)
    }
  }

  object promptText extends FxAttribute[String, PromptText]

  //==============================================================================

  implicit object ScrollPaneHasContent extends (ScrollPane Has Content) {
    def transform(v: Content): ScrollPane => Unit = { scrollPane =>
      scrollPane.setContent(v.v)
    }
  }

  implicit object TabHasContent extends (Tab Has Content) {
    def transform(v: Content): Tab => Unit = { tab =>
      tab.setContent(v.v)
    }
  }

  object content extends FxAttribute[Node, Content]

  //======================================================================

  implicit object regionHasMinSize extends (Region Has MinSize) {
    def transform(v: MinSize): Region => Unit = { region =>
      region.setMinSize(v.v._1, v.v._2)
    }
  }

  implicit object controlHasMinSize extends (Control Has MinSize) {
    def transform(v: MinSize): Control => Unit = { control =>
      control.setMinSize(v.v._1, v.v._2)
    }
  }

  object minSize extends FxAttribute[(Double, Double), MinSize]

  //==================================================================

  implicit object LabeledHasWrapText extends (Labeled Has WrapText) {
    def transform(v: WrapText): Labeled => Unit = { labeled =>
      labeled.setWrapText(v.v)
    }
  }

  object wrapText extends FxAttribute[Boolean, WrapText]

  //===================================================================

  implicit object NodeHasVisible extends (Node Has Visible) {
    def transform(v: Visible): Node => Unit = { node =>
      node.setVisible(v.v)
    }
  }

  object visible extends FxAttribute[Boolean, Visible]

  //===================================================================

  implicit object LabeledHasText extends (Labeled Has Text) {
    def transform(v: Text): Labeled => Unit = { labeled =>
      labeled.setText(v.v)
    }
  }

  implicit object TabHasText extends (Tab Has Text) {
    def transform(v: Text): Tab => Unit = { tab =>
      tab.setText(v.v)
    }
  }

  object text extends FxAttribute[String, Text]

  //===================================================================

  implicit object ToggleHasToggleGroup extends (Toggle Has SToggleGroup) {
    def transform(v: SToggleGroup): Toggle => Unit = { toggle =>
      toggle.setToggleGroup(v.v)
    }
  }


  object toggleGroup extends FxAttribute[ToggleGroup, SToggleGroup]

  //===================================================================


  implicit object WindowHasOnHidden extends (Window Has OnHidden) {
    def transform(v: OnHidden): Window => Unit = { window =>
      window.setOnHidden(new EventHandler[WindowEvent] {
        def handle(e: WindowEvent) {
          v.v.apply(e)
        }
      })
    }
  }

  object onHidden extends FxAttribute[WindowEvent => Unit, OnHidden]

  //===================================================================

  implicit object NodeHasOnMouseClicked extends (Node Has OnMouseClicked) {
    def transform(v: OnMouseClicked): Node => Unit = { node =>
      node.setOnMouseClicked(new EventHandler[MouseEvent] {
        def handle(e: MouseEvent) {
          v.v.apply(e)
        }
      })
    }
  }

  object onMouseClicked extends FxAttribute[MouseEvent => Unit, OnMouseClicked]

  //===================================================================

  implicit object NodeHasOnMousePressed extends (Node Has OnMousePressed) {
    def transform(v: OnMousePressed): Node => Unit = { node =>
      node.setOnMousePressed(new EventHandler[MouseEvent] {
        def handle(e: MouseEvent) {
          v.v.apply(e)
        }
      })
    }
  }

  object onMousePressed extends FxAttribute[MouseEvent => Unit, OnMousePressed]

  //===================================================================

  implicit object NodeHasOnKeyReleased extends (Node Has OnKeyReleased) {
    def transform(v: OnKeyReleased): Node => Unit = { node =>
      node.setOnKeyReleased(new EventHandler[KeyEvent] {
        def handle(e: KeyEvent) {
          v.v.apply(e)
        }
      })
    }
  }


  object onKeyReleased extends FxAttribute[KeyEvent => Unit, OnKeyReleased]

  //===================================================================

  implicit object NodeHasOnMouseDragged extends (Node Has OnMouseDragged) {
    def transform(v: OnMouseDragged): Node => Unit = { node =>
      node.setOnMouseDragged(new EventHandler[MouseEvent] {
        def handle(e: MouseEvent) {
          v.v.apply(e)
        }
      })
    }
  }

  object onMouseDragged extends FxAttribute[MouseEvent => Unit, OnMouseDragged]

  //===================================================================

  implicit object ButtonBaseHasOnAction extends (ButtonBase Has OnAction) {
    def transform(v: OnAction): ButtonBase => Unit = { buttonBase =>
      buttonBase.setOnAction(new EventHandler[ActionEvent] {
        def handle(e: ActionEvent) {
          v.v.apply(e)
        }
      })
    }
  }

  object onAction extends FxAttribute[ActionEvent => Unit, OnAction]

  //===================================================================


  implicit object TooltipHasContentDisplay extends (Tooltip Has SContentDisplay) {
    def transform(v: SContentDisplay): Tooltip => Unit = { tooltip =>
      tooltip.setContentDisplay(v.v)
    }
  }

  implicit object LabeledHasContentDisplay extends (Labeled Has SContentDisplay) {
    def transform(v: SContentDisplay): Labeled => Unit = { labeled =>
      labeled.setContentDisplay(v.v)
    }
  }


  object contentDisplay extends FxAttribute[ContentDisplay, SContentDisplay]

  //===================================================================

  object onPropertyChanged {
    def :=[T, C](property: C => ObservableValue[T], listener: (ObservableValue[_ <: T], T, T) => Unit): C => Unit = { container: C =>
      property.apply(container).addListener(new ChangeListener[T] {
        override def changed(observable: ObservableValue[_ <: T], old: T, _new: T) {
          listener.apply(observable, old, _new)
        }
      })
    }
  }

  object onPropertyInvalidated {
    def :=[C](property: C => Observable, listener: Observable => Unit): C => Unit = { container: C =>
      property.apply(container).addListener(new InvalidationListener {
        override def invalidated(ob: Observable) {
          listener.apply(ob)
        }
      })
    }
  }

  //===================================================================

  //  implicit def hBoxHasSpacing[T <: HBox] = new (T Has Spacing) {
  //    override def transform(v: Spacing): T => Unit = { hBox =>
  //      hBox.setSpacing(v.v)
  //    }
  //  }

  //  implicit def vBoxHasSpacing[T <: VBox] = new (T Has Spacing) {
  //    override def transform(v: Spacing): T => Unit = { vBox =>
  //      vBox.setSpacing(v.v)
  //    }
  //  }

  implicit object HBoxHasSpacing extends (HBox Has Spacing) {
    def transform(v: Spacing): HBox => Unit = { hBox =>
      hBox.setSpacing(v.v)
    }
  }

  implicit object VBoxHasSpacing extends (VBox Has Spacing) {
    def transform(v: Spacing): VBox => Unit = { vBox =>
      vBox.setSpacing(v.v)
    }
  }

  object spacing extends FxAttribute[Int, Spacing]

  //===================================================================

  implicit object RegionHasMinHeight extends (Region Has MinHeight) {
    def transform(v: MinHeight): Region => Unit = { region =>
      region.setMinHeight(v.v)
    }
  }

  implicit object ControlHasMinHeight extends (Control Has MinHeight) {
    def transform(v: MinHeight): Control => Unit = { control =>
      control.setMinHeight(v.v)
    }
  }

  object minHeight extends FxAttribute[Double, MinHeight]

  //===================================================================


  implicit object RegionHasMaxHeight extends (Region Has MaxHeight) {
    def transform(v: MaxHeight): Region => Unit = { region =>
      region.setMaxHeight(v.v)
    }
  }

  implicit object controlHasMaxHeight extends (Control Has MaxHeight) {
    def transform(v: MaxHeight): Control => Unit = { control =>
      control.setMaxHeight(v.v)
    }
  }

  object maxHeight extends FxAttribute[Double, MaxHeight]

  //===================================================================

  implicit object RegionHasMinWidth extends (Region Has MinWidth) {
    def transform(v: MinWidth): Region => Unit = { region =>
      region.setMinWidth(v.v)
    }
  }

  implicit object ControlHasMinWidth extends (Control Has MinWidth) {
    def transform(v: MinWidth): Control => Unit = { control =>
      control.setMinWidth(v.v)
    }
  }


  object minWidth extends FxAttribute[Double, MinWidth]

  //===================================================================


  implicit object RegionHasMaxWidth extends (Region Has MaxWidth) {
    def transform(v: MaxWidth): Region => Unit = { region =>
      region.setMaxWidth(v.v)
    }
  }

  implicit object ControlHasMaxWidth extends (Control Has MaxWidth) {
    def transform(v: MaxWidth): Control => Unit = { control =>
      control.setMaxWidth(v.v)
    }
  }

  object maxWidth extends FxAttribute[Double, MaxWidth]

  //===================================================================

  implicit object RegionHasPrefWidth extends (Region Has PrefWidth) {
    def transform(v: PrefWidth): Region => Unit = { region =>
      region.setPrefWidth(v.v)
    }
  }

  implicit object ControlHasPrefWidth extends (Control Has PrefWidth) {
    def transform(v: PrefWidth): Control => Unit = { control =>
      control.setPrefWidth(v.v)
    }
  }


  object prefWidth extends FxAttribute[Double, PrefWidth]

  //===================================================================

  implicit object RegionHasPrefHeight extends (Region Has PrefHeight) {
    def transform(v: PrefHeight): (Region) => Unit = { region =>
      region.setPrefHeight(v.v)
    }
  }

  implicit object ControlHasPrefHeight extends (Control Has PrefHeight) {
    def transform(v: PrefHeight): (Control) => Unit = { control =>
      control.setPrefHeight(v.v)
    }
  }


  object prefHeight extends FxAttribute[Double, PrefHeight]

  //===================================================================

  implicit object RegionHasMaxSize extends  (Region Has MaxSize) {
    def transform(v: MaxSize): Region => Unit = { region =>
      region.setMaxSize(v.v._1, v.v._2)
    }
  }

  implicit object ControlHasMaxSize extends (Control Has MaxSize) {
    def transform(v: MaxSize): Control => Unit = { control =>
      control.setMaxSize(v.v._1, v.v._2)
    }
  }

  object maxSize extends FxAttribute[(Double, Double), MaxSize]

  //===================================================================

  implicit object RegionHasPrefSize extends (Region Has PrefSize) {
    def transform(v: PrefSize): Region => Unit = { region =>
      region.setPrefSize(v.v._1, v.v._2)
    }
  }

  implicit object ControlHasPrefSize extends (Control Has PrefSize) {
    def transform(v: PrefSize): Control => Unit = { control =>
      control.setPrefSize(v.v._1, v.v._2)
    }
  }


  object prefSize extends FxAttribute[(Double, Double), PrefSize]

  //===================================================================

  implicit object TilePaneHasPrefColumns extends (TilePane Has PrefColumns) {
    def transform(v: PrefColumns): TilePane => Unit = { tilePane =>
      tilePane.setPrefColumns(v.v)
    }
  }


  object prefColumns extends FxAttribute[Int, PrefColumns]

  //===================================================================

  implicit object NodeHasId extends (Node Has Id) {
    def transform(v: Id): Node => Unit = { node =>
      node.setId(v.v)
    }
  }

  object id extends FxAttribute[String, Id]

  //===================================================================

  implicit object ButtonBaseHasGraphic extends (ButtonBase Has Graphic) {
    def transform(v: Graphic): ButtonBase => Unit = { buttonBase =>
      buttonBase.setGraphic(new ImageView(new Image(v.v)))
    }
  }

  object graphic extends FxAttribute[String, Graphic]

  //===================================================================

  implicit object TreeViewHasShowRoot extends (TreeView[_] Has ShowRoot) {
    def transform(v: ShowRoot): TreeView[_] => Unit = { treeView =>
      treeView.setShowRoot(v.v)
    }
  }


  object showRoot extends FxAttribute[Boolean, ShowRoot]

  //===================================================================


  implicit object TilePaneHasHgap extends (TilePane Has Hgap) {
    def transform(v: Hgap): TilePane => Unit = { tilePane =>
      tilePane.setHgap(v.v)
    }
  }

  object hgap extends FxAttribute[Double, Hgap]

  //===================================================================

  implicit object TilePaneHasVgap extends (TilePane Has Vgap) {
    def transform(v: Vgap): TilePane => Unit = { tilePane =>
      tilePane.setVgap(v.v)
    }
  }


  object vgap extends FxAttribute[Double, Vgap]

  //===================================================================

  implicit object ToggleHasSelected extends (Toggle Has Selected) {
    def transform(v: Selected): Toggle => Unit = { toggle =>
      toggle.setSelected(v.v)
    }
  }

  object selected extends FxAttribute[Boolean, Selected]

  //===================================================================

  implicit object WebViewHasContextMenuEnabled extends (WebView Has ContextMenuEnabled) {
    def transform(v: ContextMenuEnabled): WebView => Unit = { webView =>
      webView.setContextMenuEnabled(v.v)
    }
  }


  object contextMenuEnabled extends FxAttribute[Boolean, ContextMenuEnabled]

  //===================================================================

  implicit object ImageViewHasImage extends (ImageView Has SImage) {
    def transform(v: SImage): ImageView => Unit = { imageView =>
      imageView.setImage(v.v)
    }
  }

  object image extends FxAttribute[Image, SImage] {
    def :=(s: String): ImageView => Unit = { imageView =>
      imageView.setImage(new Image(s))
    }

    def :=(s: InputStream): ImageView => Unit = { imageView =>
      imageView.setImage(new Image(s))
    }
  }

  //===================================================================

  implicit def TreeViewHasRoot[T] = new (TreeView[T] Has Root[T]) {
    def transform(v: Root[T]): TreeView[T] => Unit = { treeView =>
      treeView.setRoot(v.v)
    }
  }

  def root[T] = new FxAttribute[TreeItem[T], Root[T]] {}

  //===================================================================

  implicit object TreeViewHasEditable extends (TreeView[_] Has Editable) {
    def transform(v: Editable): TreeView[_] => Unit = { treeView =>
      treeView.setEditable(v.v)
    }
  }

  object editable extends FxAttribute[Boolean, Editable]

  //===================================================================

  implicit object TreeViewHasSelectionMode extends (TreeView[_] Has SSelectionMode) {
    def transform(v: SSelectionMode): TreeView[_] => Unit = { treeView =>
      treeView.getSelectionModel.setSelectionMode(v.v)
    }
  }

  object selectionMode extends FxAttribute[SelectionMode, SSelectionMode]

  //===================================================================

}

