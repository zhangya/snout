package net.oschina.oldpig.snout.example

import javafx.scene.layout.HBox
import net.oschina.oldpig.snout.{NodeFactory, FxBuilder}
import javafx.geometry.{Insets, Pos}
import FxBuilder._

object Main {

  object MyHBox extends NodeFactory[HBox] {
    override def createInstance = new HBox {
      override def layoutChildren() {
        super.layoutChildren()
      }
    }
  }

  val mediaControl = is[HBox]
  val root = {
    borderPane() {
      center := hBox(padding := (10, 10, 10, 10), alignment := Pos.BASELINE_CENTER)
      bottom := MyHBox(alignment := Pos.BASELINE_CENTER) as mediaControl
    }
  }

  def main(args: Array[String]) {
    println(root.getChildren)
    println(mediaControl.getAlignment)
  }
}
