package net.oschina.oldpig.snout

import javafx.geometry.{Insets, Pos}
import javafx.scene.control._
import javafx.scene.{Group, Parent, Node}
import javafx.scene.layout.{BorderPane, HBox, Pane, Region}
import scala.util.DynamicVariable
import javafx.animation.{Interpolator, KeyValue, KeyFrame, Timeline}
import scala.concurrent.duration.Duration
import javafx.beans.value.WritableValue
import javafx.beans.property.DoubleProperty
import java.io.InputStream
import javafx.stage.{Popup, Window}

trait FxContext {
  implicit val builderParent = new BuilderParent(null)

  implicit class SNode[T <: Node](node: T) {
    def ~[V <: Node](v: V): Seq[Node] = Seq(node, v)

    def as(ref: RefS[T]): T = {
      ref.instance = node
      node
    }
  }

  implicit class STab(tab: Tab) {
    def apply(content: => Node) = {
      tab.setContent(content)
      tab
    }
  }
  implicit class SGroup[T <: Group](group: T) {
    def apply(children: => Any) = {
      builderParent.withValue(group) {
        children
      }
      group
    }
  }
  implicit class SScrollPane[T <: ScrollPane](scrollPane: ScrollPane) {
    def apply(child: => Node) = {
      scrollPane.setContent(child)
      scrollPane
    }
  }

  implicit class SRegion[T <: Region](region: T) {
    def apply(children: => Any) = {
      builderParent.withValue(region) {
        children
      }
      region
    }
  }

  implicit class SControl[T <: Control](control: T) {
    def apply(children: => Any) = {
      builderParent.withValue(control) {
        children
      }
      control
    }
  }

  implicit class SPopup[T <: Popup](win: Popup) {
    def apply(child: Node) = {
      win.getContent.add(child)
      win
    }
  }


  trait NodeChild {
    def :=(child: Node): Node
  }

  object center extends NodeChild {
    override def :=(child: Node) = {
      builderParent.value.asInstanceOf[BorderPane].setCenter(child)
      child
    }
  }

  object bottom extends NodeChild {
    override def :=(child: Node) = {
      builderParent.value.asInstanceOf[BorderPane].setBottom(child)
      child
    }
  }

  object top extends NodeChild {
    override def :=(child: Node) = {
      builderParent.value.asInstanceOf[BorderPane].setTop(child)
      child
    }
  }

  object left extends NodeChild {
    override def :=(child: Node) = {
      builderParent.value.asInstanceOf[BorderPane].setLeft(child)
      child
    }
  }

  object right extends NodeChild {
    override def :=(child: Node) = {
      builderParent.value.asInstanceOf[BorderPane].setRight(child)
      child
    }
  }

  object unmanaged extends NodeChild {
    def :=(child: Node) = {
      child.setManaged(false)
      builderParent.value match {
        case borderPane: BorderPane => borderPane.getChildren.add(child)
      }
      child
    }
  }

}

object FxBuilder extends FxAttributes with Factories with Properties






