package net.oschina.oldpig.snout

import javafx.scene.layout._
import javafx.scene.{Parent, Node}
import javafx.scene.control._
import javafx.geometry.Insets
import javafx.scene.image.ImageView
import javafx.stage.Popup
import javafx.scene.web.WebView

trait Factory[T] {
  def createInstance: T

  def apply(attributes: (T => Unit)*): T
}

trait Customized {self: Parent =>
  def adopt(child: Node): Unit
}

trait NodeFactory[T <: Node] extends Factory[T] {
  def apply(attributes: (T => Unit)*): T = {
    import net.oschina.oldpig.snout.FxBuilder.builderParent
    val result = createInstance
    attributes.foreach(_.apply(result))
    adoptions(result)(builderParent.value)
    result
  }

  def adoptions(child: Node): PartialFunction[Parent, Unit] = {
    case null =>
    case toolbar: ToolBar => toolbar.getItems.add(child)
    case stackPane: StackPane => stackPane.getChildren.add(child)
    case vBox: VBox => vBox.getChildren.add(child)
    case hBox: HBox => hBox.getChildren.add(child)
    case splitPane: SplitPane => splitPane.getItems.add(child)
    case borderPane: BorderPane =>
    case customized: Customized => customized.adopt(child)
    case x => throw new RuntimeException("Unimplemented adoption: " + x)
  }
}




trait Factories extends FxContext {
  object hBox extends NodeFactory[HBox] {
    override def createInstance = new HBox

    object hgrow extends StaticAttribute[HBox, Priority, Hgrow]

    object margin extends StaticAttribute[HBox, Insets, Margin] {
      def :=(v: (Int, Int, Int, Int))(implicit transformer: CanSet[HBox, Margin], wrapper: Insets => Margin):Node => Unit = :=(new Insets(v._1, v._2, v._3, v._4))
    }

  }

  object gridPane extends NodeFactory[GridPane] {
    override def createInstance = new GridPane

    object constraints extends StaticAttribute[GridPane, (Int, Int), SConstraints]
  }

  object stackPane extends NodeFactory[StackPane] {
    override def createInstance = new StackPane
  }

  object popup extends Factory[Popup] {
    override def createInstance = new Popup

    override def apply(attributes: (Popup => Unit)*) = {
      val result = createInstance
      attributes.foreach(_.apply(result))
      result
    }
  }

  object label extends NodeFactory[Label] {
    override def createInstance = new Label
  }

  object textField extends NodeFactory[TextField] {
    override def createInstance = new TextField()
  }


  object vBox extends NodeFactory[VBox] {
    override def createInstance = new VBox
    object vgrow extends StaticAttribute[VBox, Priority, Vgrow]
  }

  object button extends NodeFactory[Button] {
    override def createInstance = new Button
  }

  object contextMenu extends Factory[ContextMenu] {
    override def createInstance = new ContextMenu

    override def apply(attributes: (ContextMenu => Unit)*) = {
      val result = createInstance
      attributes.foreach(_.apply(result))
      result
    }
  }

  object scrollPane extends NodeFactory[ScrollPane] {
    def createInstance = new ScrollPane
  }

  object webView extends NodeFactory[WebView] {
    def createInstance = new WebView
  }

  object tilePane extends NodeFactory[TilePane] {
    def createInstance = new TilePane
  }

  object tabPane extends NodeFactory[TabPane] {
    def createInstance = new TabPane
  }

  object tab extends Factory[Tab] {
    def createInstance = new Tab

    def apply(attributes: (Tab => Unit)*) = {
      val result = createInstance
      attributes.foreach(_.apply(result))
      builderParent.value.asInstanceOf[TabPane].getTabs.add(result)
      result
    }
  }


  object toggleButton extends NodeFactory[ToggleButton] {
    override def createInstance = new ToggleButton()
  }

  def treeView[T] = new NodeFactory[TreeView[T]] {
    override def createInstance = new TreeView[T]()
  }

  object imageView extends NodeFactory[ImageView] {
    override def createInstance = new ImageView()
  }

  object region extends NodeFactory[Region] {
    override def createInstance = new Region
  }


  object borderPane extends NodeFactory[BorderPane] {
    override def createInstance = new BorderPane
  }


  object splitPane extends NodeFactory[SplitPane] {
    override def createInstance = new SplitPane
  }

  object toolBar extends NodeFactory[ToolBar] {
    override def createInstance = new ToolBar
  }

  object anchorPane extends NodeFactory[AnchorPane] {
    override def createInstance = new AnchorPane
  }

  def is[N <: Node] = new RefS[N] {
    override var instance: N = null.asInstanceOf[N]
  }

}
