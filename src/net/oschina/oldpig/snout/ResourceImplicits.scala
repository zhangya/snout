package net.oschina.oldpig.snout

trait ResourceImplicits { self =>
  implicit class stringAsResource(v:String) {
    def relative = self.getClass.getResource(v).toExternalForm
  }
}
