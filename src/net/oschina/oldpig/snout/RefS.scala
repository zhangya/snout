package net.oschina.oldpig.snout

import javafx.scene.Node

trait RefS[N <: Node] {
  var instance: N
}

object RefS {
  implicit def unbox[N <: Node](ref: RefS[N]) = ref.instance
}
